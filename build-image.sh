#!/bin/bash -e

REGISTRY="RFProdV2ACR"
IMAGE_NAME="platform-services/mongoku"
VERSION="1"

az acr build --image ${IMAGE_NAME}:${VERSION} --image ${IMAGE_NAME}:latest --registry ${REGISTRY} ./
